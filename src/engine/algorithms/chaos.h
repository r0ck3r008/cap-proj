#ifndef CHAOS_H
#define CHAOS_h

#include"utils/global_defs.h"

struct image *encrypt_chirikov(struct image *, void *);
struct image *decrypt_chirikov(struct image *, void *);

#endif
