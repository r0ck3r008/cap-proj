gcc encrypt_rc4_320.c -lm -o encrypt_rc4_320
gcc encrypt_rc4_640.c -lm -o encrypt_rc4_640
gcc encrypt_rc4_800.c -lm -o encrypt_rc4_800 

gcc encrypt_vignere_320.c -lm -o encrypt_vignere_320
gcc encrypt_vignere_640.c -lm -o encrypt_vignere_640
gcc encrypt_vignere_800.c -lm -o encrypt_vignere_800

gcc encrypt_chaos_320.c -lm -o encrypt_chaos_320
gcc encrypt_chaos_640.c -lm -o encrypt_chaos_640
gcc encrypt_chaos_800.c -lm -o encrypt_chaos_800

gcc decrypt_rc4_320.c -lm -o decrypt_rc4_320
gcc decrypt_rc4_640.c -lm -o decrypt_rc4_640
gcc decrypt_rc4_800.c -lm -o decrypt_rc4_800 

gcc decrypt_vignere_320.c -lm -o decrypt_vignere_320
gcc decrypt_vignere_640.c -lm -o decrypt_vignere_640
gcc decrypt_vignere_800.c -lm -o decrypt_vignere_800

gcc decrypt_chaos_320.c -lm -o decrypt_chaos_320
gcc decrypt_chaos_640.c -lm -o decrypt_chaos_640
gcc decrypt_chaos_800.c -lm -o decrypt_chaos_800
